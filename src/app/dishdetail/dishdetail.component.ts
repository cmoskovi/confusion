import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Dish } from '../shared/dish';
import { DishService } from '../services/dish.service'
import { Params, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { switchMap } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Comment } from '../shared/comment';


@Component({
  selector: 'app-dishdetail',
  templateUrl: './dishdetail.component.html',
  styleUrls: ['./dishdetail.component.scss']
})
export class DishdetailComponent implements OnInit {
  @ViewChild('fform', {static: false}) feedbackFormDirective;


  dish: Dish;
  dishIds: string[];
  prev: string;
  next: string;

  feedbackForm: FormGroup;
  comment:Comment;


 formErrors = {
    'author': '',
    'comment': ''
    };
    validationMessages = {
      'author': {
        'required':      'Author Name is required.',
        'minlength':     'Author Name must be at least 2 characters long.'       
      },
      'comment': {
        'required':      'Comment is required.'      
      }};
  constructor(
    private dishService:DishService, 
    private route:ActivatedRoute, 
    private location:Location,
    private fb:FormBuilder
    ){ 
    this.createFeedbackForm();
  }

  createFeedbackForm(){
    this.feedbackForm = this.fb.group({
      author:['', [Validators.required, Validators.minLength(2)]],
      comment:['', [Validators.required, Validators.minLength(2)]],
      rating: 5
    });
    this.feedbackForm.valueChanges.subscribe(data => this.onValueChanged(data));  
  }
  onValueChanged(data?: any) {
    console.log('inside value changed');
    if (!this.feedbackForm) { return; }
    const form = this.feedbackForm;
    for (const field in this.formErrors) {
      if (this.formErrors.hasOwnProperty(field)) {
        // clear previous error message (if any)
        this.formErrors[field] = '';
        const control = form.get(field);
        if (control && control.dirty && !control.valid) {
          const messages = this.validationMessages[field];
          for (const key in control.errors) {
            if (control.errors.hasOwnProperty(key)) {
              this.formErrors[field] += messages[key] + ' ';
              console.log(this.formErrors[field]);
            }
          }
        }
      }
    }
  }
  ngOnInit() {
    this.dishService.getDishIds().subscribe(dishIds => this.dishIds = dishIds);
    this.route.params.pipe(switchMap((params: Params) => this.dishService.getDish(params['id'])))
    .subscribe(dish => { this.dish = dish; this.setPrevNext(dish.id); });
  }

  setPrevNext(dishId: string) {
    const index = this.dishIds.indexOf(dishId);
    this.prev = this.dishIds[(this.dishIds.length + index - 1) % this.dishIds.length];
    this.next = this.dishIds[(this.dishIds.length + index + 1) % this.dishIds.length];
  }
onSubmit() {
    console.log(new Date());
    this.comment = this.feedbackForm.value;
    this.comment.date = new Date().toDateString();
    this.dish.comments.push(this.feedbackForm.value);
    this.feedbackForm.reset({
      'author': '',
      'rating': 5,
      'comment': ''
    });
    this.feedbackFormDirective.resetForm();
  }
  formatLabel(value: number | null) {
    if (!value) {
      return 0;
    }

    if (value >= 1000) {
      return Math.round(value / 1000) + 'k';
    }

    return value;
  }
  goBack(): void {
    this.location.back();
  }

}
